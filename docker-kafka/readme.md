
## 组件
### kafka监控
* https://github.com/danielqsj/kafka_exporter

## 参考资料
* 官网链接：
  * https://github.com/sheepkiller/kafka-manager-docker
* 单机多个节点
  * https://github.com/zoidbergwill/docker-compose-kafka
* 集群：
  * https://github.com/gten/docker-kafka-cluster
  * https://cutejaneii.wordpress.com/2017/07/24/docker-11-%E7%94%A8docker%E5%BB%BA%E7%AB%8Bkafka-clustermulti-host/
* 监控工具
  * https://cutejaneii.wordpress.com/2017/07/27/kafka-2-kafka%E7%9B%A3%E6%8E%A7%E9%96%8B%E6%BA%90%E5%B7%A5%E5%85%B7kafka-offset-monitor/
  
## 启动搭建教程
*  https://segmentfault.com/a/1190000011042318
* https://cloud.tencent.com/developer/article/1151408
```
 ./kafka-server-start.sh -daemon  ../config/server.properties
  ./kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test20171116
```
  
## 操作步骤及命令
```
到(192.168.0.1)
 docker run --restart=always -d -p 9092:9092 -e KAFKA_HOST=192.168.0.1 -e KAFKA_PORT=9092 \
-e ZOOKEEPER_CONNECT=192.168.0.1:2181,192.168.0.2:2181,192.168.0.3:2181 -e KAFKA_ID=0 --name kafka-1 jeygeethan/kafka-cluster

docker run --restart=always -d -p 9093:9092 -e KAFKA_HOST=192.168.0.1 -e KAFKA_PORT=9093 \
-e ZOOKEEPER_CONNECT=192.168.0.1:2181,192.168.0.2:2181,192.168.0.3:2181 -e KAFKA_ID=1 --name kafka-2 jeygeethan/kafka-cluster

移到192.168.0.2
$ docker run --restart=always -d -p 9092:9092 -e KAFKA_HOST=192.168.0.2 -e KAFKA_PORT=9092 \
-e ZOOKEEPER_CONNECT=192.168.0.1:2181,192.168.0.2:2181,192.168.0.3:2181 -e KAFKA_ID=2 --name kafka-3 jeygeethan/kafka-cluster

$ docker run --restart=always -d -p 9093:9092 -e KAFKA_HOST=192.168.0.2 -e KAFKA_PORT=9093 \
-e ZOOKEEPER_CONNECT=192.168.0.1:2181,192.168.0.2:2181,192.168.0.3:2181 -e KAFKA_ID=3 --name kafka-4 jeygeethan/kafka-cluster

最後是192.168.0.3：
$ docker run --restart=always -d -p 9092:9092 -e KAFKA_HOST=192.168.0.3 -e KAFKA_PORT=9092 \
-e ZOOKEEPER_CONNECT=192.168.0.1:2181,192.168.0.2:2181,192.168.0.3:2181 -e KAFKA_ID=4 --name kafka-5 jeygeethan/kafka-cluster

$ docker run --restart=always -d -p 9093:9092 -e KAFKA_HOST=192.168.0.3 -e KAFKA_PORT=9093 \
-e ZOOKEEPER_CONNECT=192.168.0.1:2181,192.168.0.2:2181,192.168.0.3:2181 -e KAFKA_ID=5 --name kafka-6 jeygeethan/kafka-cluster

```

